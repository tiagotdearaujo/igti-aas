package com.aas.trabalho;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class Controller {
	
	private final Repositorio repositorio;

	private static final String CLIENT_ID = "";
	private static final String CLIENT_SECRET = "";
	
	@GetMapping("/conta/{idConta}") // ENDPOINT 01
	public ResponseEntity<Object> conta(@PathVariable Long idConta) {
		Optional<Conta> c = repositorio.findById(idConta); // Pesquisa a conta pelo id no repositório
		
		if(c.isEmpty()) { // Se a conta não foi localizada o usuário é redirecionado para a página do facebook para verificar a autorização
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.set("Location",
					"https://www.facebook.com/v3.3/dialog/oauth"
					+ "?client_id=" + CLIENT_ID
					+ "&redirect_uri=http://localhost:8080/conta/cadastro/" + idConta + "/"
					+ "&state=Teste");
			
			return ResponseEntity
					.status(HttpStatus.TEMPORARY_REDIRECT) // 307 - Redirecionamento Temporário
					.headers(responseHeaders)
					.build();
		} else { // Se a conta existir os dados dela são exibidos
			return ResponseEntity.ok(c.get());
		}
	}
	
	@GetMapping("/conta/cadastro/{idConta}") // ENDPOINT 02
	public ResponseEntity<Object> cadastro(@PathVariable Long idConta, String code, String state) throws Exception {
		// Após autorizado pelo usuário o facebook irá enviar o código de autorização (code)
		
		// Com o código de autorização solicitamos o token
		JsonNode tokenMapper = readFacebookResponse(
    		"https://graph.facebook.com/v3.3/oauth/access_token"
    		+ "?client_id=" + CLIENT_ID
    		+ "&redirect_uri=http://localhost:8080/conta/cadastro/" + idConta + "/"
    		+ "&state=" + state
    		+ "&client_secret=" + CLIENT_SECRET
    		+ "&code=" + code
	    );
		if(tokenMapper.get("error") != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(tokenMapper.get("error"));
		}
	    
		// Após receber o token solicitamos os dados do usuário
	    JsonNode userDataMapper = readFacebookResponse(
			"https://graph.facebook.com/debug_token?" +
    		"input_token=" + tokenMapper.get("access_token").textValue() +
    		"&access_token=" + CLIENT_ID + "|" + CLIENT_SECRET
	    );
		if(userDataMapper.get("error") != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(tokenMapper.get("error"));
		}
	    
		// Após receber os dados do usuário solicitamos o nome e id do facebook
	    JsonNode userMapper = readFacebookResponse(
    		"https://graph.facebook.com/" +
    				userDataMapper.get("data").get("user_id").textValue() + "/?" +
    		"access_token=" + tokenMapper.get("access_token").textValue()
	    );
		if(userMapper.get("error") != null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(tokenMapper.get("error"));
		}
	    
	    Conta c = new Conta();
	    c.setIdFacebook(userMapper.get("id").textValue());
	    c.setNome(userMapper.get("name").textValue());
	    repositorio.save(c); // Salva as informações no repositório
	    
	    HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("Location", "/conta/" + idConta);
		
		return ResponseEntity
				.status(HttpStatus.TEMPORARY_REDIRECT) // 307 - Redirecionamento Temporário
				.headers(responseHeaders)
				.build();
	}
	
	public JsonNode readFacebookResponse(String url) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		URL obj = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Content-Type", "application/json");
		
		BufferedReader buffer;
		if (connection.getResponseCode() >= 400) { // O Facebook retorna 400 quando os dados são inválidos
			buffer = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
		} else {
			buffer = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		}
		
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = buffer.readLine()) != null) {
		    content.append(inputLine);
		}
		buffer.close();
		
		connection.disconnect();
		
		return mapper.readTree(content.toString());
	}
}
