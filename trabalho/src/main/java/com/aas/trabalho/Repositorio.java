package com.aas.trabalho;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repositorio extends JpaRepository<Conta, Long> {

}
